package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Equipo;

public interface IEquipoService {
	
	//Metodos del CRUD
		public List<Equipo> listarEquipo();
		
		public Equipo guardarEquipo(Equipo equipo);	
		
		public Equipo equipoXID(String id);
		
		public Equipo actualizarEquipo(Equipo equipo); 
		
		public void eliminarEquipo(String id);

}
