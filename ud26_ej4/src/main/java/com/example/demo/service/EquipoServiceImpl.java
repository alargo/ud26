package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IEquipoDao;
import com.example.demo.dto.Equipo;

@Service
public class EquipoServiceImpl implements IEquipoService {

	
	@Autowired
	IEquipoDao equipoDAO;
	
	@Override
	public List<Equipo> listarEquipo() {
		return equipoDAO.findAll();
	}

	@Override
	public Equipo guardarEquipo(Equipo equipo) {
		return equipoDAO.save(equipo);
	}

	@Override
	public Equipo equipoXID(String id) {
		return equipoDAO.findById(id).get();
	}

	@Override
	public Equipo actualizarEquipo(Equipo equipo) {
		return equipoDAO.save(equipo);
	}

	@Override
	public void eliminarEquipo(String id) {
		equipoDAO.deleteById(id);
		
	}

}
