package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Reserva;

public interface IReservaService {
	
	public List<Reserva> listarReservas(); //Listar All 
	
	public Reserva guardarReserva(Reserva reserva);	//Guarda un Curso CREATE
	
	public Reserva reservaXID(int id); //Leer datos de un Curso READ
	
	public Reserva actualizarReserva(Reserva reserva); //Actualiza datos del Curso UPDATE
	
	public void eliminarReserva(int id);// Elimina el Curso DELETE
}
