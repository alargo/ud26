package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="venta")
public class Venta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="cajero")
	private Cajero cajero;
	
	@ManyToOne
	@JoinColumn(name="producto")
	private Productos producto;
	
	@ManyToOne
	@JoinColumn(name="maquina")
	private MaquinaRegistradora maquina;
	


	/**
	 * 
	 */
	public Venta() {
	}


	

	public Venta(int id, Cajero cajero, Productos producto, MaquinaRegistradora maquina) {
		this.id = id;
		this.cajero = cajero;
		this.producto = producto;
		this.maquina = maquina;
	}




	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Cajero getCajero() {
		return cajero;
	}



	public void setCajero(Cajero cajero) {
		this.cajero = cajero;
	}



	public Productos getProducto() {
		return producto;
	}



	public void setProducto(Productos producto) {
		this.producto = producto;
	}



	public MaquinaRegistradora getMaquina() {
		return maquina;
	}



	public void setMaquina(MaquinaRegistradora maquina) {
		this.maquina = maquina;
	}




	@Override
	public String toString() {
		return "Venta [id=" + id + ", cajero=" + cajero + ", producto=" + producto + ", maquina=" + maquina + "]";
	}

	
	

	
	
	
}
