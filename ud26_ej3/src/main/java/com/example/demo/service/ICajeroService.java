package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Cajero;



public interface ICajeroService {

	
	//Metodos del CRUD
			public List<Cajero> listarCajero(); //Listar All 
			
			public Cajero guardarCajero(Cajero cajero);	//Guarda un Curso CREATE
			
			public Cajero CajeroXID(int codigo); //Leer datos de un Curso READ
			
			public Cajero actualizarCajero(Cajero cajero); //Actualiza datos del Curso UPDATE
			
			public void eliminarCajero(int codigo);// Elimina el Curso DELETE
}
