package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IMaquinasRegistradorasDAO;
import com.example.demo.dto.MaquinaRegistradora;

@Service
public class MaquinaRegistradoraServiceImpl implements IMaquinaRegistradoraService{

	@Autowired
	IMaquinasRegistradorasDAO iMaquinasRegistradorasDAO;
	
	
	@Override
	public List<MaquinaRegistradora> listarMaquinaRegistradora() {
		return iMaquinasRegistradorasDAO.findAll();
	}

	@Override
	public MaquinaRegistradora guardarMaquinaRegistradora(MaquinaRegistradora maquinaRegistradora) {
		return iMaquinasRegistradorasDAO.save(maquinaRegistradora);
	}

	@Override
	public MaquinaRegistradora MaquinaRegistradoraXID(int codigo) {
		return iMaquinasRegistradorasDAO.findById(codigo).get();
	}

	@Override
	public MaquinaRegistradora actualizarMaquinaRegistradora(MaquinaRegistradora maquinaRegistradora) {
		return iMaquinasRegistradorasDAO.save(maquinaRegistradora);
	}

	@Override
	public void eliminarMaquinaRegistradora(int codigo) {
		iMaquinasRegistradorasDAO.deleteById(codigo);
		
	}

}
