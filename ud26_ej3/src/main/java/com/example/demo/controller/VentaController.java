package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Venta;
import com.example.demo.service.VentaServiceImpl;


@RestController
@RequestMapping("/api")
public class VentaController {

	@Autowired
	VentaServiceImpl ventaService;
	
	
	@GetMapping("/ventas")
	public List<Venta> listarPiezaProveedor(){
		return ventaService.listarVentas();
	}
	
	
	@PostMapping("/ventas")
	public Venta salvaVenta(@RequestBody Venta venta) {
		
		return ventaService.guardarVenta(venta);
	}
	
	
	@GetMapping("/ventas/{id}")
	public Venta ventasXCodigo(@PathVariable(name="id") int id) {
		
		Venta RegistroVenta_xCodigo= new Venta();
		
		RegistroVenta_xCodigo=ventaService.ventaXID(id);
		
		System.out.println("RegistroVenta XCodigo: "+RegistroVenta_xCodigo);
		
		return RegistroVenta_xCodigo;
	}
	
	@PutMapping("/ventas/{id}")
	public Venta actualizarPiezaProveedor(@PathVariable(name="id")int id,@RequestBody Venta venta) {
		
		Venta venta_seleccionado= new Venta();
		Venta venta_actualizado= new Venta();
		
		venta_seleccionado= ventaService.ventaXID(id);
		
		
		venta_seleccionado.setCajero(venta.getCajero());
		venta_seleccionado.setMaquina(venta.getMaquina());
		venta_seleccionado.setProducto(venta.getProducto());
		
		venta_actualizado = ventaService.actualizarVenta(venta);
		
		System.out.println("La venta actualizada es: "+ venta_actualizado);
		
		return venta_actualizado;
	}
	
	@DeleteMapping("/ventas/{id}")
	public void eleiminarPiezaProveedor(@PathVariable(name="id")int id) {
		ventaService.eliminarVenta(id);;
	}
}
