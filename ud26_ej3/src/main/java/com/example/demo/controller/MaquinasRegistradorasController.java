package com.example.demo.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MaquinaRegistradora;
import com.example.demo.service.MaquinaRegistradoraServiceImpl;



@RestController
@RequestMapping("/api")
public class MaquinasRegistradorasController {

	@Autowired
	MaquinaRegistradoraServiceImpl maquinaRegistradoraService;
	
	@GetMapping("/maquinaRegistradora")
	public List<MaquinaRegistradora> listarMaquinaRegistradora(){
		return maquinaRegistradoraService.listarMaquinaRegistradora();
	}
	
	
	@PostMapping("/maquinaRegistradora")
	public MaquinaRegistradora salvarmaquinaRegistradora(@RequestBody MaquinaRegistradora maquinaRegistradora) {
		
		return maquinaRegistradoraService.guardarMaquinaRegistradora(maquinaRegistradora);
	}
	
	
	@GetMapping("/maquinaRegistradora/{codigo}")
	public MaquinaRegistradora MaquinaRegistradoraXID(@PathVariable(name="codigo") int codigo) {
		
		MaquinaRegistradora MaquinaRegistradora_xid= new MaquinaRegistradora();
		
		MaquinaRegistradora_xid = maquinaRegistradoraService.MaquinaRegistradoraXID(codigo);
		
		System.out.println("maquinaRegistradora XID:  " + MaquinaRegistradora_xid);
		
		return MaquinaRegistradora_xid;
	}
	
	@PutMapping("/maquinaRegistradora/{codigo}")
	public MaquinaRegistradora actualizarMaquinaRegistradora(@PathVariable(name="codigo")int codigo,@RequestBody MaquinaRegistradora maquinaRegistradora) {
		
		MaquinaRegistradora maquinaRegistradora_seleccionado= new MaquinaRegistradora();
		MaquinaRegistradora maquinaRegistradora_actualizado= new MaquinaRegistradora();
		
		maquinaRegistradora_seleccionado= maquinaRegistradoraService.MaquinaRegistradoraXID(codigo);
		
		maquinaRegistradora_seleccionado.setPiso(maquinaRegistradora.getPiso());
		
		maquinaRegistradora_actualizado = maquinaRegistradoraService.actualizarMaquinaRegistradora(maquinaRegistradora);
		
		System.out.println("La pieza actualizada es: "+ maquinaRegistradora_actualizado);
		
		return maquinaRegistradora_actualizado;
	}
	
	@DeleteMapping("/maquinaRegistradora/{codigo}")
	public void eliminarMaquinaRegistradora(@PathVariable(name="codigo")int codigo) {
		maquinaRegistradoraService.eliminarMaquinaRegistradora(codigo);
	}
}
