package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity
@Table(name="cientificos")
public class Cientifico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dni")
	private String dni;
	@Column(name="nomapels")
	private String nomapels;

	@OneToMany
	@Column(name="id")
	private List<Asignado> cientifico;

	
	public Cientifico() {
	}


	public Cientifico(String dni, String nomapels, List<Asignado> cientifico) {
		this.dni = dni;
		this.nomapels = nomapels;
		this.cientifico = cientifico;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getNomapels() {
		return nomapels;
	}


	public void setNomapels(String nomapels) {
		this.nomapels = nomapels;
	}

	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Asignado")
	public List<Asignado> getCientifico() {
		return cientifico;
	}


	public void setCientifico(List<Asignado> cientifico) {
		this.cientifico = cientifico;
	}


	@Override
	public String toString() {
		return "Cientifico [dni=" + dni + ", nomapels=" + nomapels + "]";
	}


	
	
	
	
	
	

	



	
	
	
		
}
