package com.example.demo.service;

import java.util.List;


import com.example.demo.dto.Proyecto;


public interface IProyectoService {

	//Metodos del CRUD
			public List<Proyecto> listarProyecto(); //Listar All 
			
			public Proyecto guardarProyecto(Proyecto proyecto);	//Guarda un Curso CREATE
			
			public Proyecto proyectoXID(char id); //Leer datos de un Curso READ
			
			public Proyecto actualizarProyecto(Proyecto proyecto); //Actualiza datos del Curso UPDATE
			
			public void eliminarProyecto(char id);// Elimina el Curso DELETE
	
}
