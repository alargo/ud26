package com.example.demo.service;

import java.util.List;


import com.example.demo.dto.Proveedor;



public interface IProveedorService {

	
	//Metodos del CRUD
			public List<Proveedor> listarProveedores(); //Listar All 
			
			public Proveedor guardarProveedor(Proveedor proveedor);	//Guarda un Curso CREATE
			
			public Proveedor proveedorXID(String id); //Leer datos de un Curso READ
			
			public Proveedor actualizarProveedor(Proveedor proveedor); //Actualiza datos del Curso UPDATE
			
			public void eliminarProveedor(String id);// Elimina el Curso DELETE
}
