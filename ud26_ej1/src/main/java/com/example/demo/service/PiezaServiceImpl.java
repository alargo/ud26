package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPiezaDAO;
import com.example.demo.dto.Pieza;

@Service
public class PiezaServiceImpl implements IPiezaService{

	@Autowired
	IPiezaDAO piezaDao;
	
	@Override
	public List<Pieza> listarPiezas() {
		return piezaDao.findAll();
	}

	@Override
	public Pieza guardarPieza(Pieza pieza) {
		return piezaDao.save(pieza);
	}

	@Override
	public Pieza piezaXID(int id) {
		return piezaDao.findById(id).get();
	}

	@Override
	public Pieza actualizarPieza(Pieza pieza) {
		return piezaDao.save(pieza);
	}

	@Override
	public void eliminarPieza(int id) {
		piezaDao.deleteById(id);
		
	}

}
