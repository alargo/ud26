package com.example.demo.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Pieza;
import com.example.demo.service.PiezaServiceImpl;


@RestController
@RequestMapping("/api")
public class PiezaController {

	@Autowired
	PiezaServiceImpl piezaService;
	
	@GetMapping("/pieza")
	public List<Pieza> listarPiezas(){
		return piezaService.listarPiezas();
	}
	
	
	@PostMapping("/pieza")
	public Pieza salvarPieza(@RequestBody Pieza pieza) {
		
		return piezaService.guardarPieza(pieza);
	}
	
	
	@GetMapping("/pieza/{codigo}")
	public Pieza piezaXID(@PathVariable(name="codigo") int id) {
		
		Pieza pieza_xid= new Pieza();
		
		pieza_xid=piezaService.piezaXID(id);
		
		System.out.println("Pieza XID: "+pieza_xid);
		
		return pieza_xid;
	}
	
	@PutMapping("/pieza/{codigo}")
	public Pieza actualizarCurso(@PathVariable(name="codigo")int id,@RequestBody Pieza pieza) {
		
		Pieza pieza_seleccionado= new Pieza();
		Pieza pieza_actualizado= new Pieza();
		
		pieza_seleccionado= piezaService.piezaXID(id);
		
		pieza_seleccionado.setNombre(pieza.getNombre());
		
		pieza_actualizado = piezaService.actualizarPieza(pieza);
		
		System.out.println("La pieza actualizada es: "+ pieza_actualizado);
		
		return pieza_actualizado;
	}
	
	@DeleteMapping("/pieza/{codigo}")
	public void eleiminarCurso(@PathVariable(name="codigo")int id) {
		piezaService.eliminarPieza(id);;
	}
}
